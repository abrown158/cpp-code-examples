# My C++ Code Snippets
This is a selection of C++ code examples covering a number of topics I have found interesting or useful.

Each example resides in its own directory, can be compiled and ran. If you open this project in vs code then it should be possible to run the examples.

The c and cpp examples can be built with gcc

See readme in individual directories for more detailed notes

## Virtual Destructor
When using inheritance in c++ virtual destructors are important 

## Overload the char* operator
the char* operator is useful when an object might be converted to a string.