# Overloading the char* operator
Sometimes an object can be serialized directly to a string in an obvious way.

Logging seems like a good example but i prefer to use a `toLog()` method for most log points.

Lets consider an object that represents a phone number. It might have many methods such as `getAreaCode()` but I can see someone wanting to write that number to a string stream or something similar for example:
```
PhoneNumber homeNumber("1234567890");
std::cout << "users phone number is:" << phoneNumber;
```

This is where overloading the `char*` operator comes in useful. The example is a very crude custom string class. I don't think I would ever write a string class but it does illustrate how to implement the operator overload nicely.