#include <iostream>

class MyCrazyStringClass
{
public:
    MyCrazyStringClass(std::string stringIn)
    {
        buffer = stringIn; // Yes I know its a copy but meh!
    }

    const char* c_str(void)
    {
        return buffer.c_str();
    }

    // If we comment this out then we get a compile failure
    // due to the line where we do std::cout << class << endl
    operator const char* ()
    {
        return buffer.c_str();
    }

private:
    std::string buffer;
};

int main (void)
{
    std::cout << "begin" << std::endl;

    MyCrazyStringClass crazyString("moo");

    std::cout <<  "contents of crazy string: "<< crazyString.c_str() << std::endl;
    
    // Next line only works if we overload char* operator
    std::cout <<  "contents of crazy string: "<< crazyString << std::endl;
    
}