#include <iostream>

class Base
{
    public:
    Base()
    {
        std::cout << "Constructing Base" <<std::endl;
    }

    //virtual //ensure this is uncommented to fix, comment out to brake 
    ~Base()
    {
        std::cout << "Destroying Base" <<std::endl;
    }
};

class Derrived : public Base
{
    public:
    Derrived()
    {
        std::cout << "Constructing Derrived" <<std::endl;
    }

    ~Derrived()
    {
        std::cout << "Destroying Derrived" <<std::endl;
    }
};

void DeleteBaseType (Base *toDelete)
{
    delete toDelete;
}

int main (void)
{
    std::cout << "Hello, World" << std::endl;
    Derrived *derrived = new Derrived();
    DeleteBaseType (derrived);
    return 0;
}
