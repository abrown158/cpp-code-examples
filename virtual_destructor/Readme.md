# Virtual destructors
Inheritance and polymorphism are a powerful features in object orientated languages. C++ provides both these features.

## Why?
I like using base classes and abstract base classes for objects for many reasons. Probably the biggest reason is that it allows dependency injection to be used which enabled mocking and therefore allows testable code to be written.

There is a sting in the tail here. If you do not implement a virtual destructor then it is possible for an objects destructor to not be called and this has the potential to leek memory.

## Interacting with the example
Compile and run the example with and without line 11 commented out. 
With the line uncommented and the virtual keyword present, you will see that all the destructors are called in the correct order

With the virtual keyword commented out, you will see that the destructor for the derived class is never called.